<!-- markdownlint-disable-file -->
# Release notes

## 2.1.4_1.0.20240202

### What's Changed

#### Fixes:
* Make sure no metadata is passed to metadata object for JSON sidecar
  files if `bids_sidecar` is `n`, so that gear does not try to upload
  nonexistent JSON sidecar file
* Log warning if `bids_sidecar` is `n` and `save_sidecar_as_metadata`
  is `n` that no BIDS sidecar information will be saved.  Suggest
  user either set `bids_sidecar` to `y` or `save_sidecar_as_metadata`
  to `y`.

#### Maintenance:
* Update unit test coverage to check that metadata object is not
  returned for JSON sidecar files when `bids_sidecar` is `n`.
* Update unit test coverage to check that warning is logged when
  both `bids_sidecar` and `save_sidecar_as_metadata` are `n`.

## 2.1.3_1.0.20240202

### What's Changed

#### Maintenance:
* Update `fw-gear-file-metadata-importer` dependency to 1.5.2
  * Limits `PerFrameFunctionalGroupsSequence` metadata to first frame

## 2.1.2_1.0.20240202

### What's Changed

#### Fixes:
* If a tag is provided in the configuration "tag", classification is no longer lost.  Previously, if a tag was provided, any previous .metadata.json file was lost.  Now, the tag is added to .metadata.json along with the existing data.

## 2.1.1_1.0.20240202

### What's Changed

#### Fixes:
* Data from sidecar is now attached as metadata on NIfTI files if save_sidecar_as_metadata is used as opposed to DICOM tags (which is close
 to the sidecar data but the units are different because BIDS expects seconds while DICOM tags are in milliseconds).

## 2.1.0_1.0.20240202

### What's Changed

#### Enhancements:
* Clarified `bids_sidecar` config value logging
* Added `save_sidecar_as_metadata` config option to allow for BIDS backwards compatibility
* If `save_sidecar_as_metadata` not explicitly set, gear checks `project.info` to identify if the project is utilizing a BIDS workflow that depends on the sidecar info being saved as metadata and sets gear behavior accordingly.
* Updated `fw-file` to 3.0.1 and `file-metadata-importer` to 1.4.0
* Updated upstream `dcm2niix` to 1.0.20240202


## 2.0.3_1.0.20230411

### What's Changed

#### Fixes:
* Fixed `bids_sidecar` "n" and "o" configuration options to work as expected


## 2.0.2_1.0.20230411

### What's Changed

#### Fixes:
* Removed "y/n" argument duplication

#### Enhancements:
* Tags input file with "dcm2niix-failure" if the gear fails
* Removes "dcm2niix-failure" tag from input file if present and gear succeeds



## 2.0.1_1.0.20230411

### What's Changed

* Updated dcm2niix to the latest version from Rorden Lab, v1.0.20230411 11-April-2023

## 2.0.0_1.0.20220720

### What's Changed

* Pydeface has been removed from this gear, with a separate Pydeface gear available [here](https://gitlab.com/flywheel-io/scientific-solutions/gears/pydeface).
* Nipype version has been updated.
* Docker image builds with pip and `requirements.txt` instead of poetry.
* Docker utilizes .dockerignore instead of specific files being notated in the Dockerfile.
* Docker utilizes `flywheel/python` instead of neurodebian.
* Docker install packages locked.
* Docs updated and lightly reformatted.
* Additional unit tests added to improve code coverage.
* .mvec files are now outputted when applicable to match .bval and .bvec handling.
* Metadata generated from the DICOM header is now stored in `file.info.header.dicom`.
* Sidecar information is no longer duplicated into the generated metadata. Make sure `bids_sidecar` configuration is set to `y` to maintain this information for use in BIDS.

## 1.4.1_1.0.20211006

### Fixes

* Extract metadata that is compatible with file-metadata-importer
* All outputs are not correctly saved when rescaling is happening

### Enhancements

* Added configuration to sanitize output filename if output filename using infile name
* Added debug mode within gear
* Gear default options now align with dcm2niix defaults.
  Default filename changed to `%f_%p_%t_%s`. Default lossless scaling changed
  `original`.

### Maintenance

* Publish to PyPi

## 1.3.4_1.0.20211006

### Fixes

* Fix merge2d option argument.

### Maintenance

* Reformat release notes.

## 1.3.3_1.0.20211006

### Fixes

* Escape metacharacters on fname. Temporary fix nipype dcm2niix interface.
Metacharacters ([]*) in fields used for output name don't break gear. @huiqiantan.
* Adjust check for sidecar to output image match. Switch metadata generation to naming
fix as well. @naterichman.

### Enhancements

* Series Description is now optional.
* merge2d option default now set to auto (merge2d = "2").
* json sidecar now included by default (bids_sidecar = 'y').

### Maintenance

* Migrated to GitLab from [GitHub](https://github.com/flywheel-apps/dcm2niix).
* Transitioned to Poetry dependency management.
* Light refactoring of some wrapper code.

## 1.3.0_1.0.20201102

### What's Changed

* Changes to accept nested inputs @joinontrue (#9)
* Add contributing notes @NPann (#8)

### Impact On Gear Output

There should be none.

## 1.2.2_1.0.20201102

### What's Changed

* Refactor repository to make pip installable gear package. @margaretmahan (#7)

### Impact On Gear Output

None.

## 1.2.1_1.0.20201102

### Fixes

BUG: TypeError: Object of type 'bytes' is not JSON serializable. Occurs when the JSON
sidecar produced by the dcm2niix tool contains bytes, which need to be decoded.
FIX: Resolve minor path issues.

### Enhancements

MAIN: Refactor script to use JSON sidecar as leading the metadata application, instead
of based on image output files.
ENH: Add local tests directory with default config files for testing; in addition to a
gear building script.

### Impact On Gear Output

None unless the user selected "ignore-errors" from the dcm2niix tool; then bypassing
downstream issues with that has now been fixed.

### Availability

On the Exchange

### Upgrade Recommendation

Default functionality has not changed. Therefore, an upgrade is recommended on an
as-needed basis.

## 1.2.0_1.0.20201102

### What's Changed

MAIN: Rename compress_nifti config option to compress_images.
MAIN: Cleanse and collate logic on ignoring errors and empty image output scenarios.
BUG: Fix the UnicodeDecodeError in metadata.py
BUG: Update logic to handle the case where bids_sidecar='o'
DOC: Update the gear label and gear description to explain inputs and outputs more
clearly.
DOC: Clarify outputs given specific configuration options: compress_images,
output_nrrd, etc.
DOC: Add a flowchart of the Gear workflow.

### Impact On Gear Output

NRRD output resolved when de-selecting compression configuration.

## 1.1.0_1.0.20201102

### What's Changed

ENH: Upgrade dcm2niix algorithm version to latest.
ENH: Incorporate additional dcm2niix command options including NRRD format, comments,
and verbose logging.
DOC: Updates to README regarding metadata captured from this Gear.

### Impact On Gear Output

dcm2niix tool: The most applicable dcm2niix version upgrade includes fixes for BIDS
RepetitionTimeExcitation and RepetitionTimeInversion tags (rordenlab/dcm2niix#439)

dcm2niix Gear: New output format, NRRD, available. Implementation of the configuration
options to add comments and verbose logging.

## 1.0.0_1.0.20200331

Initial release of dcm2niix Flywheel Gear with PyDeface.
