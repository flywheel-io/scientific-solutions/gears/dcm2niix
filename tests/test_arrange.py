"""Testing for functions within arrange.py script."""

import filecmp
import os
import tarfile
import zipfile
from pathlib import Path

import pytest

from fw_gear_dcm2niix.dcm2niix import arrange

ASSETS_DIR = Path(__file__).parent / "assets"


@pytest.fixture
def clean_filename_mocked(mocker):
    mock_clean = mocker.patch("fw_gear_dcm2niix.dcm2niix.arrange.clean_filename")

    mock_clean.return_value = "test_file"

    return mock_clean


@pytest.mark.parametrize(
    "version, ext",
    [
        ("dicom_nested_one_level", "zip"),
        ("dicom_nested_two_levels", "zip"),
        ("dicom_nested_uneven", "zip"),
        ("dicom_single", "tgz"),
        ("parrec_single", "tgz"),
    ],
)
def test_PrepareDcm2niixInput_ZipTarArchive_MatchValidDataset(version, ext, tmpdir):
    """Compares output of prepare_dcm2niix_input on zip/tar input
    with known answer, valid output.
    """

    # define input file, define and populate new_dir
    infile = f"{ASSETS_DIR}/{version}.{ext}"

    # define old dir
    old_dir = f"{ASSETS_DIR}/valid_dataset/{version}"  # corresponding unzipped dir

    # populate new dir
    new_dir = arrange.prepare_dcm2niix_input(infile, False, tmpdir)

    # get info on two directories: old and new
    (file_set_old, _, _, file_name_path_dict_old) = arrange.tally_files(old_dir)
    (file_set_new, _, _, file_name_path_dict_new) = arrange.tally_files(new_dir)

    # check that the two sets of filenames (leaves, not considering nesting level) match
    assert len(file_set_new - file_set_old) == 0
    assert len(file_set_old - file_set_new) == 0

    # file-level
    for file in [
        file_
        for file_ in file_name_path_dict_new
        if ((file_ != ".DS_Store") & (file_[0:2] != "._"))
    ]:
        # compare old and new file paths, check new one has expected form
        old_file_path = file_name_path_dict_old[file]
        new_file_path = file_name_path_dict_new[file]
        assert new_file_path == os.path.join(new_dir, file)

        # check analogous files match
        out = filecmp.cmp(old_file_path, new_file_path, shallow=True)
        assert out


@pytest.mark.parametrize(
    "version, ext",
    [
        ("dicom_nested_one_level_collision", "zip"),
        ("dicom_nested_two_levels_collision", "zip"),
        ("dicom_nested_uneven_collision", "zip"),
    ],
)
def test_PrepareDcm2niixInput_Archive_CatchCollisionError(version, ext, tmpdir):
    """Tests ability to catch collisions, where there are files with the same name
    (presumably at different levels) in the input that will be mapped to the same
    level in the output.
    """

    infile = f"{ASSETS_DIR}/{version}.{ext}"

    with pytest.raises(SystemExit):
        assert arrange.prepare_dcm2niix_input(infile, False, tmpdir)


def test_PrepareDcm2niixInput_ParRecSolo_MatchValidDataset(tmpdir):
    """Compares output of prepare_dcm2niix_input on parrec_solo input
    with known answer, valid output.
    """

    infile = f"{ASSETS_DIR}/parrec_solo.PAR"
    rec_infile = f"{ASSETS_DIR}/parrec_solo.REC"

    arrange.prepare_dcm2niix_input(infile, rec_infile, tmpdir)
    valid_dir = f"{ASSETS_DIR}/valid_dataset/parrec_solo"
    out = filecmp.dircmp(tmpdir, valid_dir)

    assert not out.left_only
    assert not out.right_only


def test_PrepareDcm2niixInput_NotActuallyParRec_CatchError(tmpdir):
    """Assertion to test prepare_dcm2niix_input() exiting when
    presented with a rec_infile that isn't a .rec file.
    """

    infile = f"{tmpdir}/fake.par"
    rec_infile = f"{tmpdir}/bad_not_rec.txt"
    Path(infile).touch()
    Path(rec_infile).touch()

    with pytest.raises(SystemExit):
        assert arrange.prepare_dcm2niix_input(infile, rec_infile, tmpdir)


def test_ExitIfArchiveEmpty_EmptyZipArchive_CatchEmptyArchiveError():
    """Assertion to test whether exit_if_archive_empty() catches
    case of empty input zip file.
    """

    with pytest.raises(SystemExit) as _, zipfile.ZipFile(
        f"{ASSETS_DIR}/empty_archive.zip", "r"
    ) as archiveObj:
        assert arrange.exit_if_archive_empty(archiveObj)


def test_ExitIfArchiveEmpty_EmptyTarArchive_CatchEmptyArchiveError():
    """Assertion to test whether exit_if_archive_empty() catches
    case of empty input tarfile.
    """

    with pytest.raises(SystemExit) as _, tarfile.open(
        f"{ASSETS_DIR}/empty_archive.tgz", "r"
    ) as archiveObj:
        assert arrange.exit_if_archive_empty(archiveObj)


def test_ExitIfArchiveEmpty_NotAnArchive_CatchError():
    """Assertion to test whether exit_if_archive_empty() catches
    case of a non-archive file being passed.
    """

    with pytest.raises(SystemExit):
        assert arrange.exit_if_archive_empty(f"{ASSETS_DIR}/parrec_solo.PAR")


@pytest.mark.parametrize(
    "version",
    [
        ("dicom_nested_one_level"),
        ("dicom_nested_two_levels"),
        ("dicom_nested_uneven"),
        ("dicom_single"),
        ("parrec_single"),
    ],
)
def test_tally_files_ReturnsCorrectValues(version):
    """Compares output of arrange.tally_files() on zip/tar input
    with known answer, valid output.
    """

    file_dir = f"{ASSETS_DIR}/valid_dataset/{version}"

    # valid answers to check output against
    valid_answer = {
        "dicom_nested_one_level": {
            "file_set": {"image_1.dcm", "image_3.dcm"},
            "file_tree": [
                f"{ASSETS_DIR}/valid_dataset/dicom_nested_one_level/subdir_0/image_1.dcm",
                f"{ASSETS_DIR}/valid_dataset/dicom_nested_one_level/subdir_0/image_3.dcm",
            ],
            "file_name_path_dict": {
                "image_1.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_nested_one_level/subdir_0/image_1.dcm",
                "image_3.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_nested_one_level/subdir_0/image_3.dcm",
            },
        },
        "dicom_nested_two_levels": {
            "file_set": {"image_1.dcm", "image_3.dcm"},
            "file_tree": [
                f"{ASSETS_DIR}/valid_dataset/dicom_nested_two_levels/subdir_0/subdir_00/image_1.dcm",
                f"{ASSETS_DIR}/valid_dataset/dicom_nested_two_levels/subdir_0/subdir_01/image_3.dcm",
            ],
            "file_name_path_dict": {
                "image_1.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_nested_two_levels/subdir_0/subdir_00/image_1.dcm",
                "image_3.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_nested_two_levels/subdir_0/subdir_01/image_3.dcm",
            },
        },
        "dicom_nested_uneven": {
            "file_set": {"image_1.dcm", "image_3.dcm"},
            "file_tree": [
                f"{ASSETS_DIR}/valid_dataset/dicom_nested_uneven/subdir_0/image_1.dcm",
                f"{ASSETS_DIR}/valid_dataset/dicom_nested_uneven/subdir_0/subdir_00/image_3.dcm",
            ],
            "file_name_path_dict": {
                "image_1.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_nested_uneven/subdir_0/image_1.dcm",
                "image_3.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_nested_uneven/subdir_0/subdir_00/image_3.dcm",
            },
        },
        "dicom_single": {
            "file_set": {
                "image_1.dcm",
                "image_2.dcm",
                "image_3.dcm",
                "image_4.dcm",
                "image_5.dcm",
                "image_6.dcm",
            },
            "file_tree": [
                f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_1.dcm",
                f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_2.dcm",
                f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_3.dcm",
                f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_4.dcm",
                f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_5.dcm",
                f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_6.dcm",
            ],
            "file_name_path_dict": {
                "image_1.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_1.dcm",
                "image_2.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_2.dcm",
                "image_3.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_3.dcm",
                "image_4.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_4.dcm",
                "image_5.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_5.dcm",
                "image_6.dcm": f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_6.dcm",
            },
        },
        "parrec_single": {
            "file_set": {"parrec_single.par", "parrec_single.rec"},
            "file_tree": [
                f"{ASSETS_DIR}/valid_dataset/parrec_single/parrec_single/parrec_single.rec",
                f"{ASSETS_DIR}/valid_dataset/parrec_single/parrec_single/parrec_single.par",
            ],
            "file_name_path_dict": {
                "parrec_single.rec": f"{ASSETS_DIR}/valid_dataset/parrec_single/parrec_single/parrec_single.rec",
                "parrec_single.par": f"{ASSETS_DIR}/valid_dataset/parrec_single/parrec_single/parrec_single.par",
            },
        },
    }

    # get info on file directory
    file_set, file_tree, _, file_name_path_dict = arrange.tally_files(file_dir)

    assert file_set == valid_answer[version]["file_set"]

    file_tree.sort()
    valid_answer[version]["file_tree"].sort()
    assert file_tree == valid_answer[version]["file_tree"]

    assert file_name_path_dict == valid_answer[version]["file_name_path_dict"]


def test_FlattenDirectory_NewDir_Success(tmpdir):
    """Assertions to test that flatten_directory() successfullly
    makes a new, flattened directory from nested input.
    """

    dir_source = f"{tmpdir}/unflattened"
    Path(dir_source).mkdir(exist_ok=True)
    Path(f"{dir_source}/test_file_1.txt").touch()
    Path(f"{dir_source}/nested").mkdir(exist_ok=True)
    Path(f"{dir_source}/nested/test_file_2.txt").touch()

    dir_target = f"{tmpdir}/flattened"

    arrange.flatten_directory(dir_source, dir_target, overwrite=False)

    assert Path(f"{dir_target}/test_file_1.txt").exists()
    assert Path(f"{dir_target}/test_file_2.txt").exists()


def test_FlattenDirectory_DirExists_NoOverwrite_CatchError(tmpdir):
    """Assertion to test that flatten_directory() exits when
    the directory target already exists and overwrite=False.
    """

    dir_source = f"{tmpdir}/unflattened"
    Path(dir_source).mkdir(exist_ok=True)
    Path(f"{dir_source}/test_file_1.txt").touch()
    Path(f"{dir_source}/nested").mkdir(exist_ok=True)
    Path(f"{dir_source}/nested/test_file_2.txt").touch()

    dir_target = f"{tmpdir}/flattened"
    Path(dir_target).mkdir(exist_ok=True)

    with pytest.raises(SystemExit):
        assert arrange.flatten_directory(dir_source, dir_target, overwrite=False)


def test_FlattenDirectory_DirExists_Overwrite_Success(tmpdir):
    """Assertions to test that flatten_directory() successfully
    overwrites into already-existing directory when overwrite=True.
    """

    dir_source = f"{tmpdir}/unflattened"
    Path(dir_source).mkdir(exist_ok=True)
    Path(f"{dir_source}/test_file_1.txt").touch()
    Path(f"{dir_source}/nested").mkdir(exist_ok=True)
    Path(f"{dir_source}/nested/test_file_2.txt").touch()

    dir_target = f"{tmpdir}/flattened"
    Path(dir_target).mkdir(exist_ok=True)

    arrange.flatten_directory(dir_source, dir_target, overwrite=True)

    assert Path(f"{dir_target}/test_file_1.txt").exists()
    assert Path(f"{dir_target}/test_file_2.txt").exists()


def test_ExtractArchiveContents_NestedTarfile(tmpdir):
    """Assertion to test successful extraction of .tgz file
    into dcm2niix_input_dir.
    """

    infile = Path(f"{ASSETS_DIR}/dicom_nested.tgz")
    with tarfile.open(infile, "r") as tar_obj:
        dcm2niix_input_dir = arrange.extract_archive_contents(tar_obj, tmpdir)

    assert Path(f"{dcm2niix_input_dir}/image_1.dcm").exists()


def test_SetupDcm2niixInputDir_Success(clean_filename_mocked, tmpdir):
    """Assertions to test successful creation of dcm2niix_input_dir."""
    infile = Path(f"{tmpdir}/test_file.txt")
    infile.touch()
    work_dir = Path(f"{tmpdir}/work_dir")
    work_dir.mkdir()

    input_dir, dirname = arrange.setup_dcm2niix_input_dir(infile, work_dir)

    assert (dirname) == "test_file"
    assert (Path(input_dir)).exists()
    assert clean_filename_mocked.called


def test_CleanFilename_Extensions_Match():
    """Assertions on test cases to check file path extension cleaning
    transformation."""
    assert arrange.clean_filename("dicom.dicom.zip") == "dicom"
    assert arrange.clean_filename("dicom.dcm.tgz") == "dicom"
    assert arrange.clean_filename("parrec.parrec.zip") == "parrec"
    assert arrange.clean_filename("parrec.PAR") == "parrec"
    assert arrange.clean_filename("parrec") == "parrec"


def test_CleanFilename_Alphanumerics_Match():
    """Assertions on test cases to check underscore transformation."""
    assert arrange.clean_filename("dicom.@1.2.D.tgz") == "dicom_1_2_D"
    assert arrange.clean_filename("dicom__#!.zip") == "dicom"
    assert arrange.clean_filename("1#.files.parrec.zip") == "1_files"


def test_CleanFilename_Underscores_Match():
    """Assertions on test cases to check underscore transformation."""
    assert arrange.clean_filename("parrec__files.parrec.tgz") == "parrec_files"
    assert arrange.clean_filename("dicom__.zip") == "dicom"


def test_CleanFilename_NoAlphanumerics():
    """Assertion on test case to check transformation when
    no alphanumerics are left in cleaned filename.
    """

    assert arrange.clean_filename("#!%@.zip") == "inputs"


def test_StripPrefixZipArchive_Success():
    """Assertion to test that strip_prefix_zip_archive() correctly
    removes the subdirectory from filenames, checked against list
    of expected filenames.
    """

    test_file = zipfile.ZipFile(f"{ASSETS_DIR}/dicom_nested.zip")
    output_generator = arrange.strip_prefix_ziparchive(test_file, "subdir/")
    expected_files = [
        "image_1.dcm",
        "image_2.dcm",
        "image_3.dcm",
        "image_4.dcm",
        "image_5.dcm",
        "image_6.dcm",
    ]
    actual_files = []

    for i in range(len(expected_files)):
        actual = next(output_generator).filename
        actual_files.append(actual)

    assert set(expected_files) == set(actual_files)


def test_StripPrefixTarArchive_Success():
    """Assertion to test that strip_prefix_tar_archive() correctly
    removes the subdirectory from filenames, checked against list
    of expected filenames.
    """

    test_file = tarfile.open(f"{ASSETS_DIR}/dicom_nested.tgz")
    output_generator = arrange.strip_prefix_tararchive(test_file, "subdir")
    expected_files = [
        "image_1.dcm",
        "image_2.dcm",
        "image_3.dcm",
        "image_4.dcm",
        "image_5.dcm",
        "image_6.dcm",
    ]
    actual_files = []

    for i in range(len(expected_files)):
        actual = next(output_generator).name
        actual_files.append(actual)

    assert set(expected_files) == set(actual_files)


def test_AdjustParrecFilenames_Success(tmpdir):
    """Assertions to test that adjust_parrec_filenames() catches and
    changes .par and .rec files as expected.
    """

    input_dir = f"{tmpdir}/parrecs"
    Path(input_dir).mkdir()
    Path(f"{input_dir}/test_PAR.PAR").touch()
    Path(f"{input_dir}/test_REC.REC").touch()

    arrange.adjust_parrec_filenames(input_dir, "test")

    assert Path(f"{input_dir}/test.par").exists()
    assert Path(f"{input_dir}/test.rec").exists()
