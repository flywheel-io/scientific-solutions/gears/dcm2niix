"""Testing for functions within dcm2niix_utils.py script."""

from pathlib import Path

import pytest

from fw_gear_dcm2niix.dcm2niix import dcm2niix_utils

ASSETS_DIR = Path(__file__).parent / "assets"


def test_CoilCombine_ListNiftis_CatchError():
    """Assertion to test whether coil_combine() catches
    case of not generating coil combined data.
    """

    with pytest.raises(SystemExit):
        nifti_files = [f"{ASSETS_DIR}/test_coil_combined.nii.gz"]
        assert dcm2niix_utils.coil_combine(nifti_files)
