"""Testing for functions within prepare.py script."""

from pathlib import Path

import pytest

from fw_gear_dcm2niix.dcm2niix import prepare

ASSETS_DIR = Path(__file__).parent / "assets"


@pytest.fixture
def prepare_dcm2niix_input_mocked(mocker, tmpdir):
    mock_prepare = mocker.patch(
        "fw_gear_dcm2niix.dcm2niix.arrange.prepare_dcm2niix_input"
    )

    mock_prepare.return_value = str(tmpdir)

    return mock_prepare


@pytest.fixture
def glob_mocked(mocker):
    mock_glob = mocker.patch("glob.glob")

    mock_glob.return_value = [
        f"{ASSETS_DIR}/valid_dataset/dicom_single/dicom_single/image_1.dcm"
    ]

    return mock_glob


@pytest.fixture
def remove_incomplete_volumes_mocked(mocker):
    mock_rm = mocker.patch(
        "fw_gear_dcm2niix.dcm2niix.dcm2niix_utils.remove_incomplete_volumes"
    )

    return mock_rm


@pytest.fixture
def decompress_dicoms_mocked(mocker):
    mock_decompress = mocker.patch(
        "fw_gear_dcm2niix.dcm2niix.dcm2niix_utils.decompress_dicoms"
    )

    return mock_decompress


def test_Setup_Complete(
    prepare_dcm2niix_input_mocked,
    glob_mocked,
    remove_incomplete_volumes_mocked,
    decompress_dicoms_mocked,
    tmpdir,
):
    """Assertions to test setup() succesfully calls additional functions."""

    _ = prepare.setup(
        infile=f"{ASSETS_DIR}/dicom_single.zip",
        rec_infile=None,
        work_dir=tmpdir,
        remove_incomplete_volumes=True,
        decompress_dicoms=True,
    )

    assert remove_incomplete_volumes_mocked.called
    assert decompress_dicoms_mocked.called
    assert glob_mocked.called
    assert prepare_dcm2niix_input_mocked.called
