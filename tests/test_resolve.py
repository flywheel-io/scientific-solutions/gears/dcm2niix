"""Testing for functions within resolve.py script."""

from pathlib import Path

import pytest

from fw_gear_dcm2niix.utils import resolve


@pytest.fixture
def generate_mocked(mocker):
    mock_generate = mocker.patch("fw_gear_dcm2niix.utils.metadata.generate")

    return mock_generate


@pytest.fixture
def retain_gear_outputs_mocked(mocker):
    mock_retain = mocker.patch("fw_gear_dcm2niix.utils.resolve.retain_gear_outputs")

    return mock_retain


def test_Setup_IgnoreErrors_Success(generate_mocked, tmpdir):
    """Assertions to test setup() when ignore_errors==True."""
    work_dir = Path(tmpdir.mkdir("work_dir"))
    dcm2niix_input_dir = Path(tmpdir.mkdir("input_dir"))
    output_dir = Path(tmpdir.mkdir("output_dir"))
    Path(f"{tmpdir}/work_dir/test.nii.gz").touch()

    _ = resolve.setup(
        output_image_files=[Path(f"{tmpdir}/work_dir/test.nii.gz")],
        output_sidecar_files=[],
        work_dir=work_dir,
        dcm2niix_input_dir=dcm2niix_input_dir,
        output_dir=output_dir,
        ignore_errors=True,
    )

    assert generate_mocked.called
    assert Path(f"{tmpdir}/output_dir/test.nii.gz").exists()
    assert not (Path(f"{tmpdir}/work_dir/test.nii.gz").exists())


def test_Setup_Complete(generate_mocked, retain_gear_outputs_mocked, tmpdir):
    """Assertions to test setup() and confirm generate and
    retain_gear_outputs functions are called.
    """

    work_dir = Path(tmpdir.mkdir("work_dir"))
    dcm2niix_input_dir = Path(tmpdir.mkdir("input_dir"))
    output_dir = Path(tmpdir.mkdir("output_dir"))
    Path(f"{tmpdir}/work_dir/test.nii.gz").touch()

    _ = resolve.setup(
        output_image_files=[Path(f"{tmpdir}/work_dir/test.nii.gz")],
        output_sidecar_files=[],
        work_dir=work_dir,
        dcm2niix_input_dir=dcm2niix_input_dir,
        output_dir=output_dir,
    )

    assert generate_mocked.called
    assert retain_gear_outputs_mocked.called


def test_RetainGearOutputs_NiftiNrrdConflict_CatchError(tmpdir):
    """Assertion to check that retain_gear_outputs() exits
    if retain_nifti and output_nrrd are both True.
    """

    tmpdir.mkdir("work_dir")
    output_dir = Path(tmpdir.mkdir("output_dir"))
    Path(f"{tmpdir}/work_dir/test.nii.gz").touch()
    Path(f"{tmpdir}/work_dir/.metadata.json").touch()

    with pytest.raises(SystemExit):
        assert resolve.retain_gear_outputs(
            output_image_files=[Path(f"{tmpdir}/work_dir/test.nii.gz")],
            output_sidecar_files=[],
            output_dir=output_dir,
            retain_nifti=True,
            output_nrrd=True,
        )


def test_RetainGearOutputs_RetainNifti(tmpdir):
    """Assertions to check that retain_gear_outputs()
    appropriately moves files to the output directory.
    """

    tmpdir.mkdir("work_dir")
    output_dir = Path(tmpdir.mkdir("output_dir"))
    Path(f"{tmpdir}/work_dir/test.nii.gz").touch()
    Path(f"{tmpdir}/work_dir/.metadata.json").touch()
    Path(f"{tmpdir}/work_dir/test.json").touch()

    resolve.retain_gear_outputs(
        output_image_files=[f"{tmpdir}/work_dir/test.nii.gz"],
        output_sidecar_files=[f"{tmpdir}/work_dir/test.json"],
        output_dir=output_dir,
        retain_nifti=True,
        retain_sidecar=True,
    )

    assert Path(f"{tmpdir}/output_dir/test.nii.gz").exists()
    assert Path(f"{tmpdir}/output_dir/test.json").exists()
