"""Testing for functions within interfaces.py script."""

from contextlib import nullcontext as does_not_raise

import pytest

from fw_gear_dcm2niix.dcm2niix import interfaces


@pytest.fixture
def search_mocked(mocker):
    mock_searcher = mocker.patch("fw_gear_dcm2niix.dcm2niix.interfaces.search_files")
    mock_searcher.return_value = ["test.mvec"]

    return mock_searcher


@pytest.mark.parametrize(
    "test_input,expectation",
    [
        (0, does_not_raise()),
        (1, does_not_raise()),
        (2, does_not_raise()),
        (3, pytest.raises(Exception)),
        (True, does_not_raise()),
        ("foo", pytest.raises(Exception)),
    ],
)
def test_Dcm2niixEnhanced_merge_imgs(test_input, expectation):
    """Assertion to test modified interface for dcm2niix,
    with merge_imgs as 0/1/2 or Boolean input allowed.

    Args:
        test_input, the input to test
        expectation, does_not_raise() or pytest.raises(Exception)

    Returns:
        N/A, assertions are main point
    """

    converter = interfaces.Dcm2niixEnhanced()

    with expectation:
        converter.inputs.merge_imgs = test_input
        assert converter.inputs.merge_imgs == test_input


def test_Dcm2niixEnhanced_parse_files(search_mocked):
    converter = interfaces.Dcm2niixEnhanced()
    filename_to_test = ["test.mvec"]
    converter._parse_files(filename_to_test)

    assert converter.output_files == []
    assert converter.bvecs == []
    assert converter.mvecs == ["test.mvec"]
    assert converter.bvals == []
    assert converter.bids == []


def test_Dcm2niixEnhanced_outputs():
    output_map = dict(
        bids=dict(),
        bvals=dict(),
        bvecs=dict(),
        converted_files=dict(),
        mvecs=dict(),
    )
    outputs = interfaces.Dcm2niixEnhanced.output_spec()

    for key, metadata in list(output_map.items()):
        for metakey, value in list(metadata.items()):
            assert getattr(outputs.traits()[key], metakey) == value


def test_Dcm2niixEnhanced_parse_inputs():
    converter = interfaces.Dcm2niixEnhanced()
    converter.inputs.bids_format = True
    converter.inputs.single_file = False
    converter.inputs.verbose = True
    converter.inputs.crop = True
    converter.inputs.has_private = False
    converter.inputs.anon_bids = False
    converter.inputs.ignore_deriv = True
    converter.inputs.to_nrrd = False

    result = converter._parse_inputs()
    assert result == [
        "-ba n",
        "-b y",
        "-z y",
        "-x y",
        "-t n",
        "-i y",
        "-o .",
        "-s n",
        "-e n",
        "-v y",
    ]
