#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Main script for dcm2niix gear."""

import logging
import os
import typing as t

import flywheel_gear_toolkit
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_dcm2niix.dcm2niix import dcm2niix_run, dcm2niix_utils, prepare
from fw_gear_dcm2niix.utils import parse_config, resolve

log = logging.getLogger(__name__)


def resolve_and_tag(
    gear_context: GearToolkitContext,
    output_image_files,
    output_sidecar_files,
    dcm2niix_input_dir,
):
    # Resolve gear outputs, including metadata capture
    resolve_args = parse_config.generate_resolve_args(gear_context)
    # pop the "tag" from the "gear_args", since resolve.setup doesn't support it:
    tag = resolve_args.pop("tag", None)

    # Log warning if both retain_sidecar and save_sidecar_as_metadata are False that
    # no BIDS sidecar file or sidecar metadata will be generated.
    if (
        resolve_args["retain_sidecar"] is False
        and resolve_args["save_sidecar_as_metadata"] is False
    ):
        log.warning(
            "No BIDS sidecar file or sidecar metadata will be generated. Consider setting "
            "'save_sidecar_as_metadata' to 'y' or 'bids_sidecar' to 'y'."
        )

    metadata_to_save = resolve.setup(
        output_image_files,
        output_sidecar_files,
        str(gear_context.work_dir),
        dcm2niix_input_dir,
        gear_context.output_dir,
        **resolve_args,
    )

    # add metadata_to_save to what gear context will save into .metadata.json on exit
    for kk, vv in metadata_to_save.items():
        gear_context.metadata.update_container(kk, True, **vv)

    input_filename = gear_context.get_input_filename("dcm2niix_input")
    failure_tag = "dcm2niix-failure"
    # If there are output files and the user entered a tag, tag one of them:
    if output_image_files is not None and tag:
        first_filename = os.path.basename(output_image_files[0])
        gear_context.metadata.update_file(first_filename, tags=[t.cast(str, tag)])
    elif output_image_files is None and gear_context.config.get("tag_on_failure"):
        gear_context.metadata.update_file(input_filename, tags=[failure_tag])

    file_ = flywheel_gear_toolkit.utils.metadata.get_file(
        input_filename, gear_context, None
    )
    tags = file_.tags
    if failure_tag in tags and output_image_files is not None:
        tags.remove(failure_tag)
        gear_context.metadata.update_file(input_filename, tags=tags)


def main(gear_context: GearToolkitContext):
    """Orchestrate dcm2niix gear."""
    # Prepare dcm2niix input, which is a directory of dicom or parrec images
    prep_args = parse_config.generate_prep_args(gear_context)
    dcm2niix_input_dir = prepare.setup(**prep_args)

    # Run dcm2niix
    dcm2niix_args = parse_config.generate_dcm2niix_args(gear_context)
    output = dcm2niix_run.convert_directory(
        dcm2niix_input_dir, gear_context.work_dir, **dcm2niix_args
    )

    # Nipype interface output from dcm2niix can be a string or list (desired)
    try:
        output_image_files = output.outputs.converted_files
        if isinstance(output_image_files, str):
            output_image_files = [output_image_files]

        output_sidecar_files = output.outputs.bids
        if isinstance(output_sidecar_files, str):
            output_sidecar_files = [output_sidecar_files]

    except AttributeError:
        log.info("No outputs were produced from dcm2niix tool.")
        output_image_files = None

    # NIfTI files are assumed to be expected for coil combined
    if not gear_context.config["output_nrrd"] and output_image_files is not None:
        # Apply coil combined method
        if gear_context.config["coil_combine"]:
            dcm2niix_utils.coil_combine(output_image_files)

    # If bvals or bvecs defined, then add to the list of output image files
    if isinstance(output.outputs.bvals, str):
        output_image_files.append(output.outputs.bvals)

    if isinstance(output.outputs.bvals, list):
        output_image_files.extend(output.outputs.bvals)

    if isinstance(output.outputs.bvecs, str):
        output_image_files.append(output.outputs.bvecs)

    if isinstance(output.outputs.bvecs, list):
        output_image_files.extend(output.outputs.bvecs)

    # Similarly, if mvecs defined, add to list of output image files
    if isinstance(output.outputs.mvecs, str):
        output_image_files.append(output.outputs.mvecs)

    if isinstance(output.outputs.mvecs, list):
        output_image_files.extend(output.outputs.mvecs)

    resolve_and_tag(
        gear_context, output_image_files, output_sidecar_files, dcm2niix_input_dir
    )

    exit_status_local = 0

    return exit_status_local


if __name__ == "__main__":
    with GearToolkitContext() as context:
        context.init_logging()
        log = logging.getLogger(__name__)

        try:
            exit_status = main(context)
        except Exception as e:
            if context.config.get("tag_on_failure"):
                input_filename = context.get_input_filename("dcm2niix_input")
                context.metadata.update_file(input_filename, tags=["dcm2niix-failure"])
            raise e

    log.info("Successful dcm2niix gear execution with exit status %s.", exit_status)
